package main

import (
  "fmt"
  "net/http"
  "github.com/gorilla/mux"
  "io"
  "io/ioutil"
  "os"
  "regexp"
  "strings"
)

func handleCats(w http.ResponseWriter, r *http.Request) {
  vars := mux.Vars(r)
  name, namePresent := vars["name"]

  w.Header().Set("Content-Type", "application/json")

  if(!namePresent) {
    getAllCats(w ,r)
    return
  }

  sanitizedName := sanitizeName(name)

  switch r.Method {
  case http.MethodGet:
    getCat(w, r, sanitizedName)
  case http.MethodPost:
    postCat(w, r, sanitizedName)
  case http.MethodPut:
    putCat(w, r, sanitizedName)
  case http.MethodDelete:
    deleteCat(w, r, sanitizedName)
  }
}

func handleStaticCats(w http.ResponseWriter, r *http.Request) {
  vars := mux.Vars(r)
  name := sanitizeName(vars["name"])
  http.ServeFile(w, r, "./images/" + name)
}

func setupAndServeRoutes() {
  r := mux.NewRouter()

  r.HandleFunc("/cats", handleCats).Methods("GET")
  r.HandleFunc("/cats/{name}", handleCats).Methods("GET", "POST", "PUT", "DELETE")
  r.HandleFunc("/images/{name}", handleStaticCats).Methods("GET")

  http.ListenAndServe(":8080", r)
}

func main() {
  ensureImagesDirectoryExists()
  setupAndServeRoutes()
}

func getAllCats(w http.ResponseWriter, r *http.Request) {
  files, err := ioutil.ReadDir("./images")
  if err != nil {
    handleResponse(w, http.StatusInternalServerError, `"` + err.Error() + `"`)
    return
  }

  var cats []string

  for _, f := range files {
    cats = append(cats, `"` + f.Name() + `"`)
  }

  handleResponse(w, http.StatusOK, "[" + strings.Join(cats, ",") + "]")
}

func getCat(w http.ResponseWriter, r *http.Request, name string) {
  if _, err := os.Stat(imagePath(name)); os.IsNotExist(err) {
    handleResponse(w, http.StatusNotFound, `"Cat does not exist."`)
    return
  }

  handleResponse(w, http.StatusOK, `"http://localhost:8080/images/` + name + `"`)
}

func postCat(w http.ResponseWriter, r *http.Request, name string) {
  // TODO: does this _actually_ protect against files larger than 10mb?
  r.ParseMultipartForm(10 << 20)
  formFile, _, err := r.FormFile("image")
  if err != nil {
    handleResponse(w, http.StatusInternalServerError, `"` + err.Error() + `"`)
    return
  }

  if _, err := os.Stat(imagePath(name)); !os.IsNotExist(err) {
    handleResponse(w, http.StatusForbidden, `"Cat already exists. Please update with PUT."`)
    return
  }

  defer formFile.Close()

  fileHeader := make([]byte, 512)
  if _, err := formFile.Read(fileHeader); err != nil {
    handleResponse(w, http.StatusInternalServerError, `"` + err.Error() + `"`)
    return
  }
  contentType := http.DetectContentType(fileHeader)

  if(!acceptableFileFormat(contentType)) {
    handleResponse(w, http.StatusUnsupportedMediaType, `"Please only upload cats as PNGs, GIFs, or JPGs."`)
    return
  }

  if _, err := formFile.Seek(0, 0); err != nil {
    handleResponse(w, http.StatusInternalServerError, `"` + err.Error() + `"`)
    return
  }

  serverFile, err := os.OpenFile(imagePath(name), os.O_WRONLY|os.O_CREATE, 0666)
  if err != nil {
    handleResponse(w, http.StatusInternalServerError, `"` + err.Error() + `"`)
    return
  }
  defer serverFile.Close()

  io.Copy(serverFile, formFile)

  handleResponse(w, http.StatusOK, `"Cat uploaded successfully."`)
}

// TODO: in general, but specifically in this function, we repeat a lot of code.
// Ideally, we wouldn't do this, but because of Go's procedural nature, I'm not
// certain how to abstract effectively. For example, the most repeated code is
// error handling, but that needs to be in each function because "return" cannot
// be bubbled up from other functions. In Ruby, I'd probably use a pattern
// like raising errors from private methods that bubble up to a rescue in
// the public methods.
func putCat(w http.ResponseWriter, r *http.Request, name string) {
  if _, err := os.Stat(imagePath(name)); os.IsNotExist(err) {
    handleResponse(w, http.StatusNotFound, `"Cat does not exist. Use POST instead."`)
    return
  }

  r.ParseMultipartForm(10 << 20)
  formFile, _, err := r.FormFile("image")
  if err != nil {
    handleResponse(w, http.StatusInternalServerError, `"` + err.Error() + `"`)
    return
  }

  defer formFile.Close()

  fileHeader := make([]byte, 512)
  if _, err := formFile.Read(fileHeader); err != nil {
    handleResponse(w, http.StatusInternalServerError, `"` + err.Error() + `"`)
    return
  }
  contentType := http.DetectContentType(fileHeader)

  if(!acceptableFileFormat(contentType)) {
    handleResponse(w, http.StatusUnsupportedMediaType, `"Please only upload cats as PNGs, GIFs, or JPGs."`)
    return
  }

  if _, err := formFile.Seek(0, 0); err != nil {
    handleResponse(w, http.StatusInternalServerError, `"` + err.Error() + `"`)
    return
  }

  err = os.Remove(imagePath(name))

  if err != nil {
	   handleResponse(w, http.StatusInternalServerError, `"` + err.Error() + `"`)
     return
	}

  serverFile, err := os.OpenFile(imagePath(name), os.O_WRONLY|os.O_CREATE, 0666)
  if err != nil {
    handleResponse(w, http.StatusInternalServerError, `"` + err.Error() + `"`)
    return
  }
  defer serverFile.Close()

  io.Copy(serverFile, formFile)

  handleResponse(w, http.StatusOK, `"Cat updated successfully."`)
}

func deleteCat(w http.ResponseWriter, r *http.Request, name string) {
  if _, err := os.Stat(imagePath(name)); os.IsNotExist(err) {
    handleResponse(w, http.StatusNotFound, `"Cat does not exist."`)
    return
  }

  err := os.Remove(imagePath(name))

  if err != nil {
	   handleResponse(w, http.StatusInternalServerError, `"` + err.Error() + `"`)
     return
	}

  handleResponse(w, http.StatusOK, `"Cat deleted successfully."`)
}

func ensureImagesDirectoryExists() {
  if _, err := os.Stat("./images"); os.IsNotExist(err) {
    os.Mkdir("./images", 0700)
  }
}

// TODO: verify that this approach is sufficient. We probably need more checks
// to validate that this is an image, but this is outside exercise scope.
func acceptableFileFormat(contentType string) bool {
  acceptableContentTypes := []string{"image/gif", "image/jpeg", "image/png"}
  for _, acceptableContentType := range acceptableContentTypes {
		if contentType == acceptableContentType {
      return true
    }
	}
  return false
}

func handleResponse(w http.ResponseWriter, httpStatus int, value string) {
  w.WriteHeader(httpStatus)
  fmt.Fprint(w, `{"response":` + value + `}`)
}

// TODO: I wonder if there's a way to memoize this? It's not intensive, but
// we are nonetheless calling it multiple times.
func imagePath(name string) string {
  return "./images/" + name
}

func sanitizeName(name string) string {
  reg, _ := regexp.Compile("[^a-zA-Z]+")
  return strings.ToLower(reg.ReplaceAllString(name, ""))
}
